#!/usr/bin/python
#
#  The 1st line tells UNIX like OS variants where to find the correct Python 
#  interpreter. This is ingnored by Operating systems such as Windows
#
#  accessdate.py
#
#  A python script to determine files which have not been modified since a 
#  specified date
#
#  Read through a recursive directory listing file (currently provided on
#  sharepoint)  parse the mtime of the file and compare it to the refernce
#  date provided as a  parameter.
#
#  parse out the directory path and the filename. We may want to produce lists
#  of files but not used in V1.0
#
#   version 1.2  23/05/22   Dave Herdman
#
#   Revision History
#   V1.0    21/03/22    Initial Release Process only file totals
#   V1.1    22/04/22    Modified to produce 3 output files with the names
#                       <input-file-path>_older.txt and 
#	                    <input-file-path>_recent.txt and 
#                       <input-file-path>_stats.csv
#   V1.2    23/05/22    Modified to allow a Wildcard directory path as input for 
#                       processing multiple files
#   V1.3    27/05/22    Change to use Argparse to manage passing command line arguments
# 
#   Maintained using MS Visual Studio Code
#
#   Initialy Developed using the Pycharm IDE
#   See PyCharm help at https://www.jetbrains.com/help/pycharm/
#
#   Usage: accessdate.py <Wildcard Path of files to process> <Path-to-output-File-Directory> <report-file-name>
#
import sys
import os
import os.path
import re
import glob
import datetime
from datetime import date
from datetime import timedelta
import argparse

# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    # process the command line agruments (if any)

    parser = argparse.ArgumentParser(description="Find recently modified files & Produce stats")
    parser.add_argument("-v","--verbose", action="store_true",help="output status & debugging messages to console")
    parser.add_argument("-i","--input", type=str, required=True, help="Input file path (can contain * to select multiple files)")
    parser.add_argument("-o","--output",type=str, required=True, help="Path of directory to contain output files")
    parser.add_argument("-r","--report",type=str, required=True, help="Name (Not Path) of the csv report file ")
    parser.add_argument("-s","--start", required=True, type=datetime.date.fromisoformat, help="File listing produced date YYYY-MM-DD")
    parser.add_argument("-t","--thresh",type=int,required=True,help="Number of days from start which is threshold between live/stale")

    args = parser.parse_args()

    filesRE = args.input.replace("\\*",r"*")
    out_file_dir = args.output
    rep_file_name = args.report

    # Determine the threshold date based on the start date.
    # The Threshold determines which files have been modified "Recently"
    # in this context Recent is between the start date and the Threshold
    # date. Stale files are anything that has not been modified since the
    # threshold date
    #

    curdate = datetime.date(22,3,7)
    thresh = timedelta(args.thresh)
    thresh_date = curdate - thresh
    print("------------- Starting AccessDate run ---------------")
    print("Directory to process is %s" % filesRE)
    print("Output Directory is %s" % out_file_dir)
    print("Start Date is %s" % args.start)
    print("Threshold Date is %s" % thresh_date)
 
    

    # set pattern for a time field
    pat = re.compile(r'\d\d\:\d\d')


    # initialise number of file counters
    stale_file_cnt = 0
    live_file_cnt = 0

    # initialise the file size counters
    # These sizes will be held in gigabytes
    stale_file_size = 0
    live_file_size = 0

    # initialise count of how many input files we have processed
    files_processed = 0
    giga = 1000 * 1000 * 1000

    for listfile_name in glob.glob(filesRE):
        # Give a status message
        print("Processing file %s" % listfile_name)

        #
        # Formulate the output file name
        #
        in_file_name = os.path.basename(listfile_name)
        outfile1_name = os.path.splitext(in_file_name)[0] + "_recent.txt"
        outfile1_name = os.path.join(out_file_dir,outfile1_name)
        outfile2_name = os.path.splitext(in_file_name)[0] + "_older.txt"
        outfile2_name = os.path.join(out_file_dir,outfile2_name)
        outfile1 = open(outfile1_name,"w")
        outfile2 = open(outfile2_name, "w")
        if files_processed == 0:
            stats_file_name = os.path.join(out_file_dir,rep_file_name)
            statsfile = open(stats_file_name,"w")
            statsfile.write("Input File(environment),Total Files processed,Total live files,Total Live File size (GB),Total Stale files,Total Stale File size (GB)\n")

        #
        # Open the input file
        #
        infile = open(listfile_name,"r",encoding="latin-1")
        

        for line in infile:
            elements=line.split()

            # process the elements in a line depending on what type of filesystem
            # object we are processing. This is denoted by the first character of
            # the posix umask field as listed below
            #
            #       First Character     meaning
            #           -               regular file
            #           d               directory
            #           l               soft link
            #           c               character special file
            #                             (probably unlikely for this application)
            #
            objecttype = elements[0][0]


            # There may be white space in the file & Directory names, so we can't
            #  rely on an element number to locate these. Instead, we scan forwards
            # for the full path & backwards for the filename.
            full_path = line[line.find(r'/'):]
            #print("full path %s" % (full_path,))
            file_name = full_path[full_path.rfind(r'/')+1:]
            #print("File name is %s" % (file_name,))
            dir_path = full_path[:full_path.rfind(r'/')+1]

            if objecttype == "-":
                # it's a regular file
                pass
            if objecttype == "d":
                # it's a directory
                pass
            if objecttype == "l":
                # it's a symbolic link
                pass

            # Now obtain mtime data
            txt_file_month = elements[5]
            file_day = int(elements[6])

            # The next field in the directory listing is either a time stamp
            # for files that have been modified in the last 6 months or a
            # 4 digit year for files that have been modified more than 6 months
            # ago . For ease of processing we convert time stamps into dates as
            # the analysis we are carrying out only has a granularity of days
            #

            dto = datetime.datetime.strptime(txt_file_month,"%b")
            file_month = int(dto.month)

            if pat.match(elements[7]):
                # it is a time field so within 6 months. Note that this
                # code is curerently not general purpose and relies on
                # the current year being 2022. This needs to be made more
                # generic ASAP
                if file_month > 3:
                    # The year is 2021
                    file_year = 2021
                else:
                    file_year = 2022
            else:
                # It is a year field so more than 6 months ago
                file_year = int(elements[7])


            #
            # Now the date on the file is before the threshold we 
            # write out to the "recent" files log otherewise we
            # write it out to the "stale" files log. We also keep
            # talies of number of files and size
            #

            file_date = date(file_year,file_month,file_day)

            if file_date < thresh_date :
                # File is "stale"
                outfile2.write(line)
                stale_file_cnt += 1

                # Add to the cumulative size
                stale_file_size += int(elements[4])            

            else:
                # File is "live"
                outfile1.write(line)
                live_file_cnt += 1
                live_file_size += int(elements[4])
        
        #
        # End of processing one input file
        # Do end of file housekeeping
        # 
        outfile1.close()
        outfile2.close()
        infile.close()
        statsfile.write(in_file_name + "," + str(stale_file_cnt + live_file_cnt) + "," + \
             str(live_file_cnt) + "," + str(live_file_size/giga) + "," + str(stale_file_cnt) + "," + str(stale_file_size/giga) +"\n")
        files_processed += 1
        stale_file_cnt = 0
        stale_file_size = 0
        live_file_cnt = 0
        live_file_size = 0
    # 
	# Output finishing totals
	# Then Close all files
    # 
    statsfile.close()
	
    
   
    